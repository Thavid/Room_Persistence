package com.example.admin.libraryapp.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.Update;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ActionProvider;

import com.example.admin.libraryapp.entity.Address;
import com.example.admin.libraryapp.entity.Category;
import com.example.admin.libraryapp.entity.CategoryDao;
import com.example.admin.libraryapp.entity.User;
import com.example.admin.libraryapp.repository.AddressDao;
import com.example.admin.libraryapp.repository.UserDao;

/**
 * Created by admin on 11/24/17.
 */

@Database(entities = {User.class, Category.class, Address.class}, version = DatabaseInfo.NEXT_DATABASE_VERSION)
public abstract class AppDatabase  extends RoomDatabase {

    private  static AppDatabase db;
    public abstract UserDao userDao();
    public abstract CategoryDao categoryDao();
    public abstract AddressDao addressDao();
    public static AppDatabase newInstance(Context context){

        if(null==db){
            db = Room.databaseBuilder(context, AppDatabase.class,DatabaseInfo.DATABASE_NAME)
                    .allowMainThreadQueries()
                    //.fallbackToDestructiveMigration()
                    .addMigrations(MIGRATION)
                    .build();
        }
        return db;
    }

    public static final Migration MIGRATION = new Migration(
            DatabaseInfo.CURRENT_DATABASE_VERSION,DatabaseInfo.NEXT_DATABASE_VERSION
    ) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
          database.execSQL("" +
                   "create table address (id integer not null PRIMARY KEY);");
        }
    };

    public static  void destroy(){
        if(null!=db) db=null;
    }


}
