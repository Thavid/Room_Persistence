package com.example.admin.libraryapp.entity;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by admin on 11/24/17.
 */

@Dao
public  interface CategoryDao {

    @Query("select * from category")
    List<Category> getAll();
}
