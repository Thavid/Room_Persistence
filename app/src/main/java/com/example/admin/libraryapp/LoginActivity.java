package com.example.admin.libraryapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.admin.libraryapp.base.BaseActivity;
import com.example.admin.libraryapp.database.AppDatabase;
import com.example.admin.libraryapp.entity.User;
import com.example.admin.libraryapp.repository.UserMemoryRepository;
import com.example.admin.libraryapp.utill.UserSession;

public class LoginActivity extends AppCompatActivity{

    EditText etUserName;
    EditText etUserPassword;
    Button btnLogin;
    AppDatabase db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initObjects();
        
        etUserName= (EditText) findViewById(R.id.tvUserName);
        etUserPassword= (EditText) findViewById(R.id.tvpassword);
        btnLogin= (Button) findViewById(R.id.btnLogin);

    }

    private void initObjects() {
        db=AppDatabase.newInstance(getApplicationContext());
    }

    private boolean authentication(User user,User remoteUser){

        if(user.getName().equals(remoteUser.getName())){
            if(user.getPassword().equals(remoteUser.getPassword())){
                UserSession.saveUser(this,remoteUser);
                UserSession.checkUserSession(this);
                Intent intent= new Intent(this,MainActivity.class);
                startActivity(intent);
                finish();
                return true;
            }else {
                UserSession.checkUserSession(this);
            }
        }else {
            UserSession.checkUserSession(this);
        }

        return false;
    }

    public void inAuthentication(View view) {
        User u=new User();
        u.setPassword(etUserPassword.getText().toString());
        u.setName(etUserName.getText().toString());
        User remoteUser=db.userDao().getUser(u.getName());

        authentication(u,remoteUser);

    }
}
