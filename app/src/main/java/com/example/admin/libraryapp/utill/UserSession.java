package com.example.admin.libraryapp.utill;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.nsd.NsdManager;
import android.net.wifi.aware.PublishConfig;
import android.util.Log;

import com.example.admin.libraryapp.LoginActivity;
import com.example.admin.libraryapp.entity.User;

/**
 * Created by admin on 11/24/17.
 */

public class UserSession {

    public static final String ID="ID";
    public static final String NAME="NAME";
    public static final String PASSWORD="PASSWORD";

    public static final String USER_SESSION="USER_SESSION";
    private static User defaultUser;
    static  SharedPreferences sharedPreferences;

    public static  SharedPreferences getPreference(Context context, String fileName){
        sharedPreferences=context.getSharedPreferences(USER_SESSION,Context.MODE_PRIVATE);
        defaultUser=new User();
        defaultUser.setName("userName");
        defaultUser.setPassword("password");
        return sharedPreferences;
    }
    public  static void saveUser(Context context, User user){
        SharedPreferences preferences=getPreference(context,USER_SESSION);
        SharedPreferences.Editor editor=preferences.edit();

        editor.putInt(ID,user.getId());
        editor.putString(NAME,user.getName());
        editor.putString(PASSWORD,user.getPassword());

        editor.commit();

    }

    public static User getUserSession(Context context){
        User user=new User();

        SharedPreferences preferences=getPreference(context,USER_SESSION);
        user.setId(preferences.getInt(ID,defaultUser.getId()));
        user.setName(preferences.getString(NAME,defaultUser.getName()));
        user.setPassword(preferences.getString(PASSWORD,defaultUser.getPassword()));

        return user;
    }

    public static void logout(Context context){

        saveUser(context,defaultUser);
    }

    public static void checkUserSession(Context context){
        User user = getUserSession(context);
        Log.e("user-> ",user.toString());
        if(user.getName().equals("userName") && user.getPassword().equals("password")) {
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        }

    }

}
