package com.example.admin.libraryapp.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.admin.libraryapp.entity.User;

import java.util.List;

/**
 * Created by admin on 11/24/17.
 */

@Dao
public interface UserDao {

    @Query("select * from user")
    List<User> getAll();

    @Query("select * from user where name= :name")
    User getUser(String name);
    @Insert
    void createUser(User user);

    @Update
    void editUser(User user);
    @Delete
    void removeUser(User user);
}
