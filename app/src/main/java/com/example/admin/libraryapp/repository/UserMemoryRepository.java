package com.example.admin.libraryapp.repository;

import com.example.admin.libraryapp.entity.User;

/**
 * Created by admin on 11/24/17.
 */

public class UserMemoryRepository {

    User user;

    private UserMemoryRepository (){
        user =new User();
        user.setId(1);
        user.setName("admin");
        user.setPassword("admin");
    }
    public static UserMemoryRepository  getInstance(){

        return new UserMemoryRepository();
    }


    public  User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user=user;
    }
}
