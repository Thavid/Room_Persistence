package com.example.admin.libraryapp.database;

/**
 * Created by admin on 11/24/17.
 */

public interface DatabaseInfo {

    String DATABASE_NAME="LIBRARY_DB";
    int CURRENT_DATABASE_VERSION=3;
    int NEXT_DATABASE_VERSION=CURRENT_DATABASE_VERSION+1;

}
