package com.example.admin.libraryapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.admin.libraryapp.base.BaseActivity;
import com.example.admin.libraryapp.database.AppDatabase;
import com.example.admin.libraryapp.entity.User;
import com.example.admin.libraryapp.repository.UserMemoryRepository;
import com.example.admin.libraryapp.utill.UserSession;

public class MainActivity extends AppCompatActivity {


    static User user;
    AppDatabase db;
    UserMemoryRepository repository;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //repository=UserMemoryRepository.getInstance();
        initObjects();
        createUser();

        UserSession.checkUserSession(this);



    }

    private void initObjects() {
        db=AppDatabase.newInstance(getApplicationContext());
    }

    public void onLogout(View view) {

        UserSession.logout(this);
        UserSession.checkUserSession(this);
    }

    protected void createUser(){
        User user=new User();
        user.setName("admin");
        user.setPassword("admin");
        db.userDao().createUser(user);
    }

    public void onCreateUser(View view) {
        createUser();
    }
}
