package com.example.admin.libraryapp.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import com.example.admin.libraryapp.entity.Address;

/**
 * Created by admin on 11/24/17.
 */

@Dao
public interface AddressDao {
    @Insert
    void addAddress(Address address);
}
