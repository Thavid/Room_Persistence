package com.example.admin.libraryapp.base;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.admin.libraryapp.LoginActivity;
import com.example.admin.libraryapp.database.AppDatabase;
import com.example.admin.libraryapp.entity.User;
import com.example.admin.libraryapp.utill.UserSession;

/**
 * Created by admin on 11/24/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    AppDatabase db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db=AppDatabase.newInstance(getApplicationContext());
    }

//    protected  void checkUserSession(){
//        User user = UserSession.getUserSession(this);
//        Log.e("user-> ",user.toString());
//        if(user.getName().equals("userName") && user.getPassword().equals("password")) {
//            Intent intent = new Intent(this, LoginActivity.class);
//            startActivity(intent);
//        }
//
//    }

    protected AppDatabase getDb(){
        return db;
    }
}
